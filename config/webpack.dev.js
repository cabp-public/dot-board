const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

const pathDist = path.resolve(__dirname, './..',  './dist');
const pathDistPublic = '/';

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    contentBase: pathDist,
    publicPath: pathDistPublic,
    historyApiFallback: true
  }
});
