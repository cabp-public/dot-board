const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const dotenv = require('dotenv');

const pathSrc = path.resolve(__dirname, './..', './src');
const pathPublic = path.resolve(__dirname, './..', './public');
const pathDist = path.resolve(__dirname, './..', './dist');
const pathDistPublic = '/';

/* Environment Vars */
const envFileRead = dotenv.config();

if (envFileRead.error) {
  throw envFileRead.error;
}

const env = envFileRead.parsed;
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});

/**
 * Plugins
 */
const cleanPlugin = new CleanWebpackPlugin({
  cleanOnceBeforeBuildPatterns: [
    '**/*',
    '!.gitignore'
  ],
  cleanStaleWebpackAssets: false
});
const htmlPlugin = new HtmlWebpackPlugin({
  template: path.resolve(pathPublic, './index.html'),
  filename: 'index.html',
  favicon: path.resolve(pathPublic, './favicon.ico')
});
const terserPlugin = new TerserPlugin();
const definePlugin = new webpack.DefinePlugin(envKeys);

module.exports = {
  context: __dirname,
  entry: {
    app: path.resolve(pathSrc, './index.js'),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env'
            ],
            plugins: [
              '@babel/plugin-proposal-object-rest-spread'
            ]
          }
        }
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: 'style-loader',
            options: {
              injectType: 'linkTag',
              attributes: {
                type: 'text/css',
                media: 'screen,projection'
              }
            }
          },
          'file-loader'
        ]
      }
    ]
  },
  plugins: [
    definePlugin,
    cleanPlugin,
    htmlPlugin
  ],
  optimization: {
    minimize: true,
    minimizer: [
      terserPlugin
    ]
  },
  output: {
    filename: '[name].js',
    path: pathDist,
    publicPath: pathDistPublic
  }
};
