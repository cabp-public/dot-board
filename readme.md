# Dots Board

### Project description

1. Build a webapp using React that displays a board (a 2D matrix) and allows the user to change
locations of points inside that board.

    The user should be able to toggle the nodes, i.e. click on empty locations (marked as -) and turn
them on (marked as o) and vice versa.

    Assume the input of the webapp (the board configuration) should be from a potential GET
endpoint, and the board should be submittable with a POST endpoint. You can pick the data
format you prefer for that matter.

2. After submitting the board with the point locations, assume there’s a response from the server
   with a set of clusters of the active points (you should fake the actual response). Clusters are
   defined as the set of points within a distance.
   
   Given that response from the server, display these clusters over the board.
   
### Bonus points:
    - Size of the board can be configured
    - Responsive for different screen sizes
    - Add unit tests

