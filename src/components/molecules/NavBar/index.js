import React from 'react';

export default function Board(props) {
  const { buttons } = props;

  return (
    <div className="nav">
      <div className="row">
        <div className="column text-center">
          {
            buttons.map(button => (
              <button
                key={ button.key }
                onClick={ button.onClick }
                className="button button-blue-grey button-small"
              >
                { button.label }
              </button>
            ))
          }
        </div>
      </div>
    </div>
  );
}
