import React from 'react';

export default function Board(props) {
  const label = props.label || 'Loading';

  return (
    <div className="modal content-loader text-center">
      { label }
    </div>
  );
}
