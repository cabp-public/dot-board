import React, { useState, useEffect } from 'react';

export default function Point(props) {
  const contentAvail = {
    on: '●',
    off: '○'
  };
  const [content, setContent] = useState(true === props.on ? contentAvail.on : contentAvail.off);

  useEffect(() => {
    setContent(true === props.on ? contentAvail.on : contentAvail.off);
  });

  const onClick = event => {
    const { row, col, on, pointToggler } = props;
    pointToggler(row, col, on);
  };

  return (
    <td onClick={ onClick }>{ content }</td>
  );
}

