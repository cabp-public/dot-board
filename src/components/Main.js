require('normalize.css/normalize.css');
require('milligram/dist/milligram.min.css');
require('./../styles/dots.css');

import React, { Component } from 'react';

import HttpService from './../services/HttpService';
import Board from './organisms/Board';
import Settings from './organisms/Settings';
import NavBar from './molecules/NavBar';
import ContentLoader from './atoms/ContentLoader';

class Main extends Component {
  constructor(props) {
    super(props, null, null);

    this.http = HttpService({
      api: {
        local: {
          protocol: process.env.API_LOCAL_PROTOCOL,
          host: process.env.API_LOCAL_HOST,
          basePath: process.env.API_LOCAL_BASE_PATH
        },
        mockapi: {
          protocol: process.env.API_MOCK_PROTOCOL,
          host: process.env.API_MOCK_HOST,
          basePath: process.env.API_MOCK_BASE_PATH
        }
      },
      headers: {
        'Content-Type': 'application/json'
      }
    });
    this.state = {
      loading: true,
      loadLabel: 'Loading',
      settingsOn: false,
      cols: 5,
      rows: 5,
      boardRows: {}
    };

    this.dataSetup = this.dataSetup.bind(this);
    this.dataReset = this.dataReset.bind(this);
    this.togglePoint = this.togglePoint.bind(this);
    this.onSettingsReady = this.onSettingsReady.bind(this);
    this.onSubmitReady = this.onSubmitReady.bind(this);
    this.onClickSettings = this.onClickSettings.bind(this);
    this.onClickReset = this.onClickReset.bind(this);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  componentDidMount() {
    // Load config from external API
    this.http.get('mockapi', 'settings/2', this.onSettingsReady);
  }

  dataSetup(rows, cols, rowEnabledPoints) {
    const boardRows = {};
    const keys = Object.keys(rowEnabledPoints).map(key => Number(key));

    for (let r = 0; r < rows; r++) {
      if ((typeof boardRows[r]) === 'undefined') {
        boardRows[r] = [];
      }

      for (let c = 0; c < cols; c++) {
        boardRows[r][c] = keys.includes(r) && (true === rowEnabledPoints[r][c]);
      }
    }

    this.setState({ rows, cols, boardRows, loading: false });
  }

  dataReset() {
    const { rows, cols } = this.state;
    this.dataSetup(rows, cols, {});
  }

  togglePoint(row, col, value) {
    const { boardRows } = this.state;

    boardRows[row][col] = !value;
    this.setState({ boardRows });
  }

  onSettingsReady(config) {
    const { rows, cols } = config;
    this.dataSetup(rows, cols, {});
  }

  onSubmitReady(clusters) {
    // Clusters contains points as [column, row]
    // In order to display them in the board, we just need the points
    const { rows, cols } = this.state;
    const rowEnabledPoints = {};

    clusters.forEach(cluster => {
      cluster.forEach(point => {
        const r = point[1];
        const c = point[0];

        if ((typeof rowEnabledPoints[point[1]]) === 'undefined') {
          rowEnabledPoints[r] = [];
        }

        rowEnabledPoints[r][c] = true;
      });
    });

    this.dataSetup(rows, cols, rowEnabledPoints);
  }

  onClickSettings(e) {
    const settingsOn = !this.state.settingsOn;
    this.setState({ settingsOn });
  }

  onClickReset(e) {
    this.dataReset();
  }

  onClickSubmit(e) {
    // Submit selection to external API
    this.http.post('mockapi', 'clusters', {}, this.onSubmitReady);
    this.dataReset();
    this.setState({ loading: true, loadLabel: 'Submitting' });
  }

  render() {
    const { loading, loadLabel, settingsOn, cols, rows, boardRows } = this.state;
    const navButtons = [
      {
        key: 'btnSettings',
        label: 'settings',
        onClick: this.onClickSettings
      },
      {
        key: 'btnReset',
        label: 'reset',
        onClick: this.onClickReset
      },
      {
        key: 'btnSubmit',
        label: 'submit',
        onClick: this.onClickSubmit
      },
    ];

    return true === loading
      ? <ContentLoader label={ loadLabel }/>
      : <>
        <NavBar buttons={ navButtons }/>
        <Board
          cols={ cols }
          rows={ rows }
          boardRows={ boardRows }
          pointToggler={ this.togglePoint }
        />
        { settingsOn ? <Settings cols={ cols } rows={ rows }/> : null }
      </>
  };
}

export default Main;
