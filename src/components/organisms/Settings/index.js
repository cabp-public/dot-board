import React, { Component } from 'react';

class Settings extends Component {
  constructor(props) {
    super(props, null, null);
  }

  componentDidMount() {
  }

  onSubmit(e) {}

  render() {
    const { cols, rows } = this.props;

    return (
      <div className="modal settings container">
        <h1>Settings</h1>
        <form className="row">
          <div className="column column-50">
            <label htmlFor="nameField">Rows</label>
            <input type="text" id="nameField"/>
            <button onClick={ this.onSubmit } className="button button-blue-grey button-small">Send</button>
          </div>

          <div className="column column-50">
            <label htmlFor="nameField">Cols</label>
            <input type="text" id="nameField"/>
            <div className="float-right">
              <input type="checkbox" id="saveLocal"/>
              <label className="label-inline" htmlFor="confirmField">Save in my computer</label>
            </div>
          </div>
        </form>
      </div>
    );
  };
}

export default Settings;
