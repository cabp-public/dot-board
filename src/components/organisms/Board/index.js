import React from 'react';

import Point from './../../atoms/Point';

export default function Board(props) {
  const { boardRows, pointToggler } = props;

  return (
    <table>
      <tbody>
      {
        Object.keys(boardRows).map(key => (
          <tr key={ `row-${key}` }>
            {
              Object.keys(boardRows[key]).map(col => (
                <Point
                  key={ `${key}-${col}` }
                  row={ key }
                  col={ col }
                  on={ boardRows[key][col] }
                  pointToggler={ pointToggler }
                />
              ))
            }
          </tr>
        ))
      }
      </tbody>
    </table>
  );
}
