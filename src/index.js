import 'core-js/es/map';
import 'core-js/es/set';
import 'raf/polyfill';

import React from 'react';
import { render } from 'react-dom';

import Main from './components/Main';

render(<Main/>, document.getElementById('root'));
